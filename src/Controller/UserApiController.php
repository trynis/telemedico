<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserApiController extends AbstractController implements ApiAuthenticationController
{
    /**
     * @param array $array
     * @param string|null $error
     * @return Response
     */
    private function generateResponse(array $array, string $error = null) : Response
    {
        $data = [];
        $data['ok'] = is_null($error);
        if ($error) {
            $data['message'] = $error;
        }

        if (!empty($array)) {
            $data['data'] = $array;
        }

        $response = new Response();
        $response->setContent(json_encode($data));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/api/users", methods={"GET"})
     * @return Response
     */
    public function users()
    {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository(User::class)->findAll();
        $array = [];

        foreach ($users as $user) {
            $array[] = $user->toArray();
        }

        return $this->generateResponse($array);
    }

    /**
     * @param Request $request
     * @Route("/api/user", methods={"POST"})
     * @return Response
     */
    public function createUser(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $existingUser = $em->getRepository(User::class)->findOneBy(['login' => $request->get('login')]);
        if ($existingUser) {
            return $this->generateResponse([], "Login already in use");
        }

        $user = new User();
        $user->setLogin($request->get('login'));
        $user->setPassword(User::hashPassword($request->get('password')));

        $em->persist($user);
        $em->flush();

        return $this->generateResponse($user->toArray());
    }

    /**
     * @param int $id
     * @Route("/api/user/{id}", methods={"GET"})
     * @return Response
     */
    public function readUser(int $id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($id);

        if (!$user) {
            return $this->generateResponse([], "No such user");
        }

        return $this->generateResponse($user->toArray());
    }

    /**
     * @param Request $request
     * @param int $id
     * @Route("/api/user/{id}", methods={"POST"})
     * @return Response
     */
    public function updateUser(Request $request, int $id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($id);

        if (!$user) {
            return $this->generateResponse([], "No such user");
        }

        if ($request->get('login')) {

            $existingUser = $em->getRepository(User::class)->findOneBy(['login' => $request->get('login')]);
            if ($existingUser) {
                return $this->generateResponse([], "Login already in use");
            }

            $user->setLogin($request->get('login'));
        }

        if ($request->get('password')) {
            $user->setPassword(User::hashPassword($request->get('password')));
        }

        $em->persist($user);
        $em->flush();

        return $this->generateResponse([]);
    }

    /**
     * @param int $id
     * @Route("/api/user/{id}", methods={"DELETE"})
     * @return Response
     */
    public function deleteUser(int $id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($id);

        if (!$user) {
            return $this->generateResponse([], "No such user");
        }

        $em->remove($user);
        $em->flush();

        return $this->generateResponse([]);
    }


}