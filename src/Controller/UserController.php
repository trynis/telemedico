<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @return FormInterface
     */
    private function getRegisterForm()
    {
        return $this->createFormBuilder(new User())
            ->add('login', TextType::class)
            ->add('password', PasswordType::class)
            ->add('submit', SubmitType::class, ['label' => 'register'])
            ->getForm();
    }

    /**
     * @Route("/register", name="register", methods={"GET"})
     *
     * @param SessionInterface $session
     * @param FormInterface|null $form
     *
     * @return Response
     */
    public function register(SessionInterface $session, FormInterface $form = null)
    {
        if ($session->has('user')) {
            return $this->redirectToRoute('index');
        }

        if (null === $form) {
            $form = $this->getRegisterForm();
        }

        return $this->render('user/registration.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/register", methods={"POST"})
     *
     * @param SessionInterface $session
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function registering(SessionInterface $session, Request $request)
    {
        if ($session->has('user')) {
            return $this->redirectToRoute('index');
        }

        $form = $this->getRegisterForm();

        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->register($session, $form);
        }

        $user = $form->getData();
        $user->setPassword(User::hashPassword($user->getPassword()));

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute("registered");
    }

    /**
     * @Route("/registered", name="registered")
     *
     * @return Response
     */
    public function registered()
    {
        return $this->render('user/registered.html.twig');
    }

    /**
     * @Route("/login", name="login", methods={"GET"})
     *
     * @param SessionInterface $session
     * @param string|null $error
     *
     * @return Response
     */
    public function login(SessionInterface $session, $error = null)
    {
        if ($session->has('user')) {
            return $this->redirectToRoute('index');
        }

        $form = $this->createFormBuilder()
            ->add('login', TextType::class)
            ->add('password', PasswordType::class)
            ->add('submit', SubmitType::class, ['label' => 'login'])
            ->getForm();

        return $this->render('user/login.html.twig', [
            'form' => $form->createView(),
            'error' => $error,
        ]);
    }

    /**
     * @Route("/login", name="logging", methods={"POST"})
     *
     * @param Request $request
     * @param SessionInterface $session
     *
     * @return RedirectResponse|Response
     */
    public function logging(SessionInterface $session, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $password = User::hashPassword($request->get('form')['password']);

        $user = $em->getRepository(User::class)
            ->findOneBy(['login' => $request->get('form')['login'], 'password' => $password]);

        if (!$user) {
            return $this->login($session, "Wrong login or password");
        }

        $session->set('user', $user);

        return $this->redirectToRoute('index');
    }

    /**
     * @Route("/index", name="index")
     *
     * @param SessionInterface $session
     *
     * @return Response
     */
    public function index(SessionInterface $session)
    {
        if (!$session->has('user')) {
            return $this->redirectToRoute('login');
        }

        return $this->render('user/index.html.twig');
    }

    /**
     * @Route("/logout", name="logout")
     *
     * @param SessionInterface $session
     *
     * @return Response
     */
    public function logout(SessionInterface $session)
    {
        $session->remove('user');

        return $this->redirectToRoute('login');
    }

}